﻿using AutoMapper;
using bendapp.Dtos;
using bendapp.Entities;

namespace bendapp.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}