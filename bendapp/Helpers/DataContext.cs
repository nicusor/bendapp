﻿using Microsoft.EntityFrameworkCore;
using bendapp.Entities;

namespace bendapp.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
    }
}