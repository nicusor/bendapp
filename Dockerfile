##### BASE STAGE #####
FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base

#Set our workdir to /app
WORKDIR /app

#Expose ports
EXPOSE 50326 44392 80

##### TEST STAGE #####
FROM microsoft/dotnet:2.1-sdk AS test

#Set our workdir
WORKDIR /app/tests

#Copy our tests to the container
COPY XUnitTestProject1/. .

RUN dotnet restore "XUnitTestProject1.csproj"

#Run our tests
RUN dotnet test --logger:trx

##### BUILD STAGE #####
FROM microsoft/dotnet:2.1-sdk AS build

#Set our workdir to /src
WORKDIR /src

#Copy the project .csproj for restoration of packages
COPY ["bendapp/bendapp.csproj", "bendapp/"]

#Run dotnet restore in order to restore missing packages
RUN dotnet restore "bendapp/bendapp.csproj"

#Copy the sourcecodes to container
COPY . .

#Set our workir as /src/bendapp
WORKDIR "/src/bendapp"

#Build our application to /app
RUN dotnet build "bendapp.csproj" -c Release -o /app


##### PUBLISH STAGE #####
FROM build AS publish

#Publish our application to /app
RUN dotnet publish "bendapp.csproj" -c Release -o /app


##### FINAL STAGE #####
FROM base AS final

#Set our workdir to /app
WORKDIR /app

#Copy the results of build&publish stage to final stage container for it to be served
COPY --from=publish /app .

#Set our entrypoint to run our app with dotnet
ENTRYPOINT ["dotnet", "bendapp.dll"]