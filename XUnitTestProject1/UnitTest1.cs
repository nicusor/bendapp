using Xunit;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var result = 2 + 2;

            Assert.Equal(4, result);
        }
    }
}
